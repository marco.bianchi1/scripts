#!/usr/bin/env bash

cd ~/repos/gitlab.com/docebo/Automation/test-frameworks/databases/
git checkout release/db
git pull
cp ~/repos/gitlab.com/docebo/Automation/test-frameworks/databases/testData.sql ~/repos/gitlab.com/docebo/Automation/tools/release-db/
cd ~/repos/gitlab.com/docebo/Automation/tools/release-db/
rm -rf testData.sql.gz
gzip testData.sql
git add .
git commit -m "$1"
git push