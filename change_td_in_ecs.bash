#!/usr/bin/env bash

SERVICES=$(aws ecs --profile qa-learn list-services --cluster qawk-use1-hydra --output text | egrep "hydra-" | grep -v aamon | grep -o '[^/]*$' | sort)

for SERVICE in ${SERVICES}
do
    TD="$(echo $SERVICE | sed -e 's/hydra-/hydra-td-/')"
    aws ecs update-service --profile qa-learn --cluster qawk-use1-hydra --service ${SERVICE} --task-definition ${TD}:5 --force-new-deployment 2>&1 > /dev/null
done
