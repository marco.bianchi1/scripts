#!/usr/bin/env bash

for platform in $(redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 keys \* | grep -i "docebo.i" | grep -v mobile | sort | egrep -v "custom|peerboard|secondar|shape|w1|datalake-v3|small|Edge|ai.release|aws.release");
do 
echo "redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform db_host $(redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform db_host)"
echo "redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform db_replica_host $(redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform db_replica_host)"
echo "redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform datalake_host $(redis-cli -h poc-qa-rg01.8qckxj.ng.0001.usw2.cache.amazonaws.com -n 10 hget $platform datalake_host)"
done